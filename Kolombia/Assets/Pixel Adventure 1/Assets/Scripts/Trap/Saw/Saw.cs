﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{
    public float speedRotate;
    public float speedMoving;
    public float fMinX ;
    public float fMaxX ;
    public float fMinY;
    public float fMaxY;
    public int direction ;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        switch(direction)
        {
            case -1:
                if (transform.position.x > fMinX)
                {
                    float PosX = transform.position.x;
                    transform.position = new Vector2(PosX -= speedMoving, transform.position.y);
                    transform.Rotate(0f, 0f, speedRotate);
                }
                else direction = 1;
                break;
            case 1:
                if (transform.position.x < fMaxX)
                {
                    float PosX = transform.position.x;
                    transform.position = new Vector2(PosX += speedMoving, transform.position.y);
                    transform.Rotate(0f, 0f, -speedRotate);
                }
                else direction = -1;
                break;
            case -2:
                if (transform.position.y > fMinY)
                {
                    float PosY = transform.position.y;
                    transform.position = new Vector2(transform.position.x , PosY -= speedMoving );
                    transform.Rotate(0f, 0f, speedRotate);
                }
                else direction = 2;
                break;
            case 2:
                if (transform.position.y < fMaxY)
                {
                    float PosY = transform.position.y;
                    transform.position = new Vector2(transform.position.x , PosY += speedMoving );
                    transform.Rotate(0f, 0f, speedRotate);
                }
                else direction = -2;
                break;
        }
    }
}
