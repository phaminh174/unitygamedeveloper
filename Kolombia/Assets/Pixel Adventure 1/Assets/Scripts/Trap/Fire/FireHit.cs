﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireHit : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator _animator;
    // Start is called before the first frame update
    public LayerMask _layermask;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OnTriggerToPlayer();
    }
    public void OnTriggerToPlayer()
    {
        float PosX = transform.position.x;
        float PosY = transform.position.y;
        bool IsTriggerToPlayer = Physics2D.OverlapBox(new Vector2(PosX +0.25f, PosY+0.25f), transform.localScale , 90f , _layermask);
        if(IsTriggerToPlayer)
        {
            _animator.SetBool("IsTrigger", true);
        }
         else _animator.SetBool("IsTrigger", false);
        
    }
    
   
}
