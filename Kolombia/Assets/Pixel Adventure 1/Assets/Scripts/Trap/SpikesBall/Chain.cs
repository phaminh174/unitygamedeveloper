﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour
{
    public float SpeedRotate;
    public LayerMask WhatisBorder;
    public Transform BorderCheck;
    public bool ChangeDirection = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        SpikesRotate();


    }
    private void SpikesRotate()
    {
        
            transform.Rotate(0f, 0f, SpeedRotate , Space.World);
            ChangeDirection = Physics2D.OverlapCircle(BorderCheck.position, 0, WhatisBorder);
            if(ChangeDirection)
            {
                SpeedRotate = -SpeedRotate;
                ChangeDirection = false;
            }
        
    }
}
