﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMoving : MonoBehaviour
{
    // Start is called before the first frame update
    public float ScrollSpeed;
    private Material Material;
    private Vector2 offset = Vector2.zero;
    private void Awake()
    {
        Material = GetComponent<Renderer>().material;
    }
    // Update is called once per frame
    void Start()
    {
        offset = Material.GetTextureOffset("_MainTex");
    }
    void Update()
    {
        offset.y += ScrollSpeed * Time.deltaTime;
        Material.SetTextureOffset("_MainTex", offset);
    }
}
