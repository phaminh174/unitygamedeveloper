﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoving : MonoBehaviour
{
    // Start is called before the first frame update
    public float SpeedEnemy;
    public Animator _anim;
    int direction =1;
    float fMinx = -3.8f;
    float fMaxX = -0.73f;
    public PlayerInterract _PlayerInterract;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        switch(direction)
        {
            case 1:
                if (transform.position.x > fMinx)
                {
                    float PosX = transform.position.x;
                    transform.position = new Vector2(PosX -= SpeedEnemy, transform.position.y);
                    
                }
                else 
                {
                    direction = -1;
                    Flip();
                }
                break;
            case -1:
                if (transform.position.x < fMaxX)
                {
                    float PosX = transform.position.x;
                    transform.position = new Vector2(PosX += SpeedEnemy, transform.position.y);

                }
                else 
                {
                    direction = 1;
                    Flip();
                }
                break;
        }
    }
    void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("GroundCheck"))
        {
            CircleCollider2D _circle = gameObject.GetComponent<CircleCollider2D>();
            _circle.enabled = false;
            _anim.Play("Die");
            Destroy(gameObject, 0.5f);
        }
        else
        if(other.CompareTag("Player"))
        {
            _PlayerInterract.PlayerFail();
        }
    }
}
